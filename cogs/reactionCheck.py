import json
from discord.ext import commands


class ReactionCheck(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

        # ? wasn't sure how do handle role_config possibly being undefined if reading fails
        role_config = []

        try:
            # Reading in the 'reactionCheck.json' config file
            with open('data/reactionCheck.json') as data:
                role_config = json.load(data)
        except Exception as e:
            print(f"Could not read reactionCheck.json")
            print(e)

        # Loading the attributes as class variables
        self.server_id = role_config['serverId']
        self.role_emoji = role_config['roleEmoji']
        self.roleId = int(role_config['roleId'])
        self.role_message_channel_id = role_config['roleMessageChannel']
        self.roleMessage = int(role_config['roleMessage'])
        self.roleChannelOut = int(role_config['roleChannelOut'])
        self.roleChannelOutMessage = role_config['roleChannelOutMessage']

        # Variables that will be set after the first check
        self.guild = None
        self.channel = None
        self.role = None
        self.rule_channel = None
        self.rule_message = None

    async def grant_role(self, member):
        await member.add_roles(self.role)
        await self.channel.send(f'{self.roleChannelOutMessage} {member.display_name}!')

    async def prepare(self):
        # initialize references
        print(self.server_id)
        self.guild = self.bot.get_guild(self.server_id)
        self.channel = self.guild.get_channel(self.roleChannelOut)
        self.role = self.guild.get_role(self.roleId)
        self.rule_channel = self.bot.get_channel(self.role_message_channel_id)
        self.rule_message = await self.rule_channel.fetch_message(self.roleMessage)

    @commands.Cog.listener()
    async def on_member_join(self, member):
        if self.guild is None:
            await self.prepare()

        # search confirmation reaction
        reaction = next(
            (react for react in self.rule_message.reactions if str(react.emoji) == self.role_emoji),
            None)

        # if reaction not found
        if reaction is None:
            return

        # search for new member in existing confirmation reactions
        async for user in reaction.users():
            if user.id == member.id:
                await self.grant_role(member)
                return

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        """Watches all reactions being added"""
        # Checks if the reaction was on the message we are watching
        if payload.message_id == self.rule_message:
            # Checks if the emoji reacted is the one we are looking for
            if str(payload.emoji) == self.role_emoji:

                # if discord references are not ready yet
                if self.guild is None:
                    await self.prepare()

                # Getting the member
                member = self.guild.get_member(payload.user_id)

                # If the member does not have the role
                if self.role not in member.roles:
                    # give role to member
                    await self.grant_role(member)


def setup(bot):
    bot.add_cog(ReactionCheck(bot))
