import asyncio
import collections
import os
import youtube_dl

import discord
from discord.ext import commands


class SongData:
    def __init__(self, information: dict = None,
                 message: discord.Message = None):

        if 'entries' in information.keys():
            song_info = information['entries'][0]
        else:
            song_info = information

        self.title = song_info['title']
        self.url = song_info['webpage_url']
        self.minutes = int(song_info['duration']) // 60
        self.seconds = int(song_info['duration']) % 60

        self.queued_by = message.author
        self.text_channel = message.channel

        self.clean_message = None
        self.message = None
        self.filename = None
        self.player = None

    def set_filename(self, filename: str = None):
        self.filename = filename

    def set_player(self, player: discord.AudioSource = None):
        self.player = player

    def get_data(self):
        return (f'Title: {self.title}\nUrl: `{self.url}`'
                f'\nDuration: {self.minutes}m{self.seconds}s')

    async def send_queueing(self, ctx):
        self.clean_message = await discord.ext.commands.clean_content().convert(ctx, self.title)
        self.message = await ctx.send(f'Queueing **{self.clean_message}**')

    async def edit_queueing(self):
        await self.message.edit(content=f'Queued **{self.clean_message}**', suppress=False)


class Music(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

        self.voiceClient = None

        ytdlOptions = {
            'outtmpl': 'music/%(id)s.%(ext)s',
            'default_search': 'auto',
            # Disabled until I find time to add support
            'noplaylist': True,
            'ignoreerrors': True,
            'quiet': True,
            'extract-audio': True,
            'format': 'bestaudio',
            'progress_hooks': [self.download_hook]}

        self.ytdl = youtube_dl.YoutubeDL(ytdlOptions)
        self.filenames = list()

        self.playNextSong = asyncio.Event()
        self.playNextSongListener = self.bot.loop.create_task(
            self.audioPlayerTask())

        self.disconnect = asyncio.Event()
        self.disconnectListener = self.bot.loop.create_task(
            self.disconnectFromVoice())

        self.currPlayer = None
        self.skipVotes = set()
        self.musicQueue = collections.deque()

    def __exit__(self, exc_type, exc_value, traceback):
        """To cancel any running tasks"""
        self.playNextSongListener.cancel()
        self.disconnectListener.cancel()

    def download_hook(self, download):
        """Download hook used by youtube-dl, called once download finishes"""
        if download['status'] == 'finished':
            download['filename'] = download['filename'].replace('\\', '/')
            self.filenames.append(download['filename'])

    async def joinChannel(self, ctx):
        """Joins the user's voice channel, if not already in one"""
        if not self.voiceClient:
            userVoice = ctx.author.voice

            # If the user is in a voice channel
            if userVoice:
                self.voiceClient = await userVoice.channel.connect()

                return True
            else:
                await ctx.send('Please join a channel')

    async def leaveChannel(self):
        """Leaves the connected voice channel"""
        if self.voiceClient:
            await self.bot.change_presence()
            await self.voiceClient.disconnect()
            self.voiceClient = None
            self.musicQueue.clear()

    @commands.command()
    async def summon(self, ctx):
        """Summons the bot to the user's voice channel"""
        userVoice = ctx.author.voice

        # If the user is in a voice channel
        if userVoice:
            # Checks if the bot is connected to a voice channel
            if self.voiceClient:
                # Checks if the user's current voice channel is not equal to the bot's current voice channel
                if not self.voiceClient.channel == userVoice.channel:
                    await self.voiceClient.move_to(userVoice.channel)
                else:
                    await ctx.send('I am already in your voice channel!!!')
            else:
                await self.joinChannel(ctx)
        else:
            await ctx.send('Please join a channel')

    async def audioPlayerTask(self):
        """Plays the next song in the queue, if there is one"""
        while True:
            await self.playNextSong.wait()
            self.currPlayer = self.musicQueue.popleft()
            await self.currPlayer.text_channel.send(f'Now playing: **{self.currPlayer.title}**')

            await self.bot.change_presence(activity=discord.Game(name=self.currPlayer.title))
            self.voiceClient.play(self.currPlayer.player, after=self.play_next)
            self.playNextSong.clear()

    async def disconnectFromVoice(self):
        """Disconnects the bot from the voice channel"""
        while True:
            await self.disconnect.wait()
            await self.leaveChannel()
            self.disconnect.clear()

    def play_next(self, error):
        """Wrapper for playing the next song"""
        # Deleting the file from the bot
        os.remove(self.currPlayer.filename)

        # If there are songs in the queue
        if len(self.musicQueue) > 0:
            self.bot.loop.call_soon_threadsafe(self.playNextSong.set)
        # If there is nothing left to play
        else:
            self.currPlayer = None
            self.bot.loop.call_soon_threadsafe(self.disconnect.set)

    async def playWrap(self, ctx, songName: str = None):
        """Plays the song that is typed in, or adds it to queue"""
        if songName:
            # If the bot is not in a voice channel
            if not self.voiceClient:
                if not (await self.joinChannel(ctx)):
                    return

            # Getting the song that the user queued
            songInfo = self.ytdl.extract_info(songName, download=True)
            currSongData = SongData(songInfo, ctx.message)
            await currSongData.send_queueing(ctx)

            # Downloading the song
            self.ytdl.download([songName])

            # Getting the file name
            songFile = self.filenames.pop()

            # Creating a player object
            player = discord.FFmpegPCMAudio(songFile)

            # Updating the SongData object
            currSongData.set_filename(songFile)
            currSongData.set_player(player)

            await currSongData.edit_queueing()

            # If a song is already playing, queue it instead
            if self.voiceClient.is_playing():
                self.musicQueue.append(currSongData)
            else:
                self.currPlayer = currSongData
                clean_message = await self.bot.clean_converter.convert(ctx, currSongData.title)
                await ctx.send(f'Now playing: **{clean_message}**')

                await self.bot.change_presence(activity=discord.Game(name=self.currPlayer.title))
                self.voiceClient.play(player, after=self.play_next)

        else:
            await ctx.send(f'```.play [songName]\n\nPlays the '
                           f' song that is typed in, or adds it to queue.```')

    @commands.command()
    async def play(self, ctx, *, songName: str = None):
        await self.playWrap(ctx, songName)

    @commands.command()
    async def playing(self, ctx):
        """Prints out what song the bot has in its queue"""
        # Checks if something is playing
        if not self.currPlayer:
            await ctx.send('Nothing is playing')
            return

        queueInformation = (f'Playing: **{self.currPlayer.title}**'
                            f'\nDuration: {self.currPlayer.minutes}m{self.currPlayer.seconds}s.'
                            # f'\n<{self.player.url}>'
                            )

        # Checks if the queue is empty
        if len(self.musicQueue) == 0:
            clean_message = await self.bot.clean_converter.convert(ctx, queueInformation)
            await ctx.send(clean_message)
            return

        # To print the contents queue
        queueInformation += (f'\n\nSongs in queue:\n')
        counter = 1
        for song in self.musicQueue:
            currentSong = (f'\t{counter}) **{song.title}**\n'
                           f'\tDuration: {song.minutes}m{song.seconds}s.\n')
            # Since the message character limit is 2000, splits the message into ones within the limit
            if len(queueInformation+currentSong) > 2000:
                clean_message = await self.bot.clean_converter.convert(ctx, queueInformation)
                await ctx.send(clean_message)
                queueInformation = ''
            queueInformation += currentSong
            counter += 1
        clean_message = await self.bot.clean_converter.convert(ctx, queueInformation)
        await ctx.send(clean_message)

    @commands.command()
    async def skip(self, ctx):
        """Skips the song that the bot is currently playing"""
        # If the bot is playing something
        if self.voiceClient.is_playing():
            total_users = len(self.voiceClient.channel.members) - 1
            # If the song was queued by the user requesting the skip
            if ctx.author == self.currPlayer.queued_by:
                await ctx.send('Song was skipped')
                await self.bot.change_presence()
                self.voiceClient.stop()
                return

            # If the user wants to remove their vote
            if ctx.author in self.skipVotes:
                self.skipVotes.remove(ctx.author)
            else:
                self.skipVotes.add(ctx.author)

            if len(self.skipVotes) >= (total_users)/2:
                # If the majority have voted to skip the song
                self.voiceClient.stop()
                await ctx.send('Song was skipped')
            else:
                clean_message = await self.bot.clean_converter.convert(ctx, self.currPlayer.title)
                skip_percent = int(round((len(self.skipVotes)/(total_users)*100)))
                await ctx.send(f'**{len(self.skipVotes)}/{total_users} '
                               f'({skip_percent}%)** '
                               f'users have voted to skip **{clean_message}**')
        else:
            await ctx.send('Can\'t skip if I\'m not playing anything')

    @commands.command()
    async def resume(self, ctx):
        """Resumes playing the player"""
        if self.voiceClient.is_playing():
            await ctx.send('I\'m already playing')
        else:
            self.voiceClient.resume()
            await ctx.send('Playing resumed')

    @commands.command(pass_context=True)
    async def pause(self, ctx):
        """Pauses the player"""
        if self.voiceClient.is_playing():
            self.voiceClient.pause()
            await ctx.send('I\'ve paused the song')
        else:
            await ctx.send('I\'m already paused')

    # @commands.command()
    # async def purge(self, ctx):
    #     """Removes all items from the queue"""
    #     if len(self.musicQueue) > 0:
    #         self.musicQueue.clear()
    #         await ctx.send('Queue was purged')
    #     else:
    #         await ctx.send('There is no queue to purge')

    @commands.command()
    async def devilman(self, ctx):
        """Adds Devilman No Uta to the queue"""
        await self.playWrap(ctx, 'https://www.youtube.com/watch?v = diuexInkshA')


def setup(bot):
    bot.add_cog(Music(bot))
