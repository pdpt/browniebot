from discord.ext import commands


class Misc(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def iloveyou(self, ctx):
        """Expresses my love to the user who typed this command"""
        await ctx.send(f"I love you too, {ctx.author.mention} :heart:")


def setup(bot):
    bot.add_cog(Misc(bot))
