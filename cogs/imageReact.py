# ? rename this to addReactionsToImages
import json
import re
from discord.ext import commands


class ImageReact(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

        # Reading in the 'imageChannels.json' config file
        with open('data/imageChannels.json', 'r', encoding='utf-8') as data:
            self.image_channels = json.load(data)

        self.image_extensions = ['.png', '.jpg', '.jpeg', '.bmp', '.gif']

    def has_url(self, message):
        pattern = r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\), ]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
        return re.findall(pattern, message.content)

    def image_check(self, message):
        # Checks if the message has an image uploaded, or it has one embeded
        return message.attachments or message.embeds or self.has_url(message)

    @commands.Cog.listener()
    async def on_message(self, message):
        # Ignores the bot's messages
        if message.author == self.bot.user:
            return

        if str(message.channel.id) in self.image_channels:
            if self.image_check(message):
                for emoji in self.image_channels[str(message.channel.id)].split(' '):
                    await message.add_reaction(emoji)

    # TODO: change if statement(s) to .check() function(s)
    @commands.command()
    async def watchchannel(self, ctx, channel_id, *, emoji):
        """Watches the specified channel for any images, and reacts with all emoji given"""
        # Checks if the user is allowed to call this command
        if self.bot.is_valid(ctx.message.author.id):
            # If the given channel is not already being reacted to
            if channel_id not in self.image_channels:
                reaction_channel = ctx.guild.get_channel(int(channel_id))

                # If the channel exists
                if reaction_channel:
                    self.image_channels[str(reaction_channel.id)] = emoji
                    await ctx.send(f'Now reacting to all images in the given channel with the {emoji} emoji')
                else:
                    await ctx.send('Could not find the given channel')
                    return
            else:
                # Replaces the current emoji with the new ones
                self.image_channels[str(channel_id)] = emoji
                await ctx.send('I am already watching that channel, switching to new emoji')

            # Updates the 'imageChannels.json' config file
            with open('data/imageChannels.json', 'w') as data:
                json.dump(self.image_channels, data, ensure_ascii=False)

    @commands.command()
    async def unwatchchannel(self, ctx, channel_id):
        """Unwatches the specified channel"""
        # Checks if the user is allowed to call this command
        if self.bot.is_valid(ctx.message.author.id):
            # If the given channel is being reacted to
            if channel_id in self.image_channels:
                # Removes the channel
                del self.image_channels[channel_id]
                await ctx.send('I am no longer watching that channel')
            else:
                await ctx.send('I am not watching that channel')

            # Updates the 'imageChannels.json' config file
            with open('data/imageChannels.json', 'w') as data:
                json.dump(self.image_channels, data, ensure_ascii=False)


def setup(bot):
    bot.add_cog(ImageReact(bot))
