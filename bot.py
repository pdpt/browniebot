# !/usr/bin/python3.6
# -*- coding: utf-8 -*-

import logging
import re
import textwrap
import util

import discord
from discord.ext import commands

# Setting the bot's description
description = 'Cute Discord Bot, please don\'t bully'

# Creating the bot object
bot = commands.Bot(command_prefix=util.config['prefix'],
                   description=description)

startup_extensions = util.cogs


@bot.event
async def on_ready():
    print(textwrap.dedent(
        f"""
        Logged in as:
        {bot.user.name}
        {bot.user.id}
        ------------------
        """
    ))


@bot.event
async def on_message(message):
    """Handles any messages detected by the bot"""
    # Ignores the bot's messages
    if message.author == bot.user:
        return

    await bot.process_commands(message)


def user_is_elevated(ctx):
    # TODO: replace magic word "Sensei" with variable
    # TODO: add 'commands.check_any' helper?
    if commands.check(commands.is_owner()) or commands.has_role("Sensei"):
        return True


def is_valid(self, userId: int = None):
    """Checks if the user is one I've allowed to use special commands"""
    if userId == int(util.config['creatorId']):
        return True

    if int(userId) in util.config['validId'].split(', '):
        return True

    return False


commands.Bot.isValid = is_valid


def get_user(self, ctx, username):
    """To find the given user"""
    if username:
        # Checks if there exists a user named 'username'
        user = ctx.guild.get_member_named(username)
        if not user:
            # Converts the user to an id
            try:
                user = ctx.guild.get_member(
                    int(re.sub('[^0-9]', '', username)))
            except Exception as e:
                print(e)
                user = None
    else:
        user = ctx.author

    return user


commands.Bot.getUser = get_user

# Setting the clean converter as a member variable
commands.Bot.clean_converter = discord.ext.commands.clean_content()

if __name__ == '__main__':
    # Setting up logging
    logger = logging.getLogger('discord')
    logger.setLevel(logging.DEBUG)
    handler = logging.FileHandler(
        filename='discord.log', encoding='utf-8', mode='w')
    handler.setFormatter(logging.Formatter(
        '%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
    logger.addHandler(handler)

    # Loads all startup extensions into the bot
    for extension in startup_extensions:
        try:
            bot.load_extension(extension)
        except commands.ExtensionAlreadyLoaded:
            print(f"Could not load extension {extension} (already loaded)")
        except commands.ExtensionNotLoaded:
            print(f"Could not load extension {extension} (not loaded)")
        except commands.NoEntryPointError:
            print(f"Could not load extension {extension} (no entrypoint)")
        except commands.ExtensionFailed as fail:
            print(f"Could not load extension {extension} (startup error):")
            print(fail)
        except Exception as e:
            print(f"Could not load extension {extension} (unknown error)")
            print(e)

    # Running the bot
    bot.run(util.config['token'])
