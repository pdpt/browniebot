import os
import json
import yaml

base_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(base_path)

with open('config.yaml') as data:
    config = yaml.load(data)

with open('cogs.json') as data:
    cogs = json.load(data)
