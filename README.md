# BrownieBot

Dumb Discord bot that I'm just writing cause I don't wanna use anyone else's

## config.yaml Template

```YAML
clientId: '[YOUR ID GOES HERE]'

token: '[YOUR TOKEN GOES HERE]'

prefix: '[YOUR BOT PREFIX GOES HERE]'

creatorId: '[YOUR DISCORD ID GOES HERE]'

validId: '[VALID IDs GO HERE, SEPERATE THEM WITH ,]'

inviteURL: '[YOUR INVITE URL GOES HERE]'

wolframAPIKey: '[YOUR WOLFRAM API KEY GOES HERE]'
```

## reactionCheck.json Template
The bot will watch the `roleMessage` message on channel `roleMessageChannel`, for a *user* to add the `roleEmoji` reaction.
When this happens:
* *user* will be assigned role `roleId`.
* bot will send `roleChannelOutMessage` message on `roleChannelOut` channel.

```JSON
{
"roleEmoji": "[EMOJI THE BOT SHOULD WATCH FOR]",

"roleId": "[Id OF THE ROLE YOU WANT TO ASSIGN]",

"roleMessage": "[ID OF THE MESSAGE THE BOT SHOULD WATCH]",

"roleChannelOut": "[WHERE THE BOT SHOULD SEND THE MESSAGE SAYING THE ROLE WAS ADDED]",

"roleChannelOutMessage": "[MESSAGE THE BOT SHOULD SAY WHEN THE ROLE IS ADDED]",

"serverId": [SERVER ID GOES HERE],

"roleMessageChannel": [ID OF CHANNEL WITH roleMessage]
}
```

## Other config stuff

For cogs.json, put the cogs you want the bot to load on startup, it starts with every cog in it

For data/imageChannels.json, put the channel ids of channels you want the bot to react with images to as the key, with the emoji as the value

For data/roles.json, put the names of roles you want the bot to be able to modify, needs to be a list

For data/reactions.json, create a file with {}, the bot will populate it with data as it is obtained
